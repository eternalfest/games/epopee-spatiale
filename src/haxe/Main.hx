import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import quests.QuestManager;
import user_data.UserData;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import pretty_portals.PrettyPortals;
import maxmods.AddLink;
import maxmods.Misc;
import maxmods.FixedBackground;
import vault.Vault;
import items.Items;
import password.Password;
import mc2.Mc2;
import bat.Bat;


@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
      bugfix: Bugfix,
      debug: Debug,
      gameParams: GameParams,
      noNextLevel: NoNextLevel,
      addLink: AddLink,
      misc: Misc,
      fixedBackground: FixedBackground,
      items: Items,
      vault: Vault,
      password: Password,
      mc2: Mc2,
      bat: Bat,
      prettyPortals: PrettyPortals,
      quests: QuestManager,
      userData: UserData,
      merlin: Merlin,
      patches: Array<IPatch>,
      hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
