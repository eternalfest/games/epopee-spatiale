package bat;

import etwin.ds.Nil;
import hf.entity.shoot.BossFireBall;
import hf.entity.boss.Bat;
import hf.Hf;
import hf.mode.GameMode;
import patchman.IPatch;
import patchman.Ref;

@:build(patchman.Build.di())
class Bat {
    @:diExport
    public var change_bat_constants(default, null): IPatch;
    @:diExport
    public var change_fireball_constants(default, null): IPatch;
    @:diExport
    public var replace_bat_rotating_fireballs(default, null): IPatch;
    @:diExport
    public var direction_aware_fireball_init(default, null): IPatch;
    @:diExport
    public var direction_aware_fireball_center(default, null): IPatch;
    @:diExport
    public var prepare_to_launch_fireballs(default, null): IPatch;
    @:diExport
    public var launch_fireballs(default, null): IPatch;
    @:diExport
    public var bat_in_dim(default, null): IPatch;
    @:diExport
    public var bat_in_dim_bosslevel(default, null): IPatch;
    @:diExport
    public var bat_in_dim_checkclear(default, null): IPatch;

    public function new(config: BatConfig): Void {
        this.change_bat_constants = Ref.auto(GameMode.initGame).before(function(hf: Hf, self: GameMode): Void {
            if (config.lives != null)
                hf.entity.boss.Bat.LIVES = config.lives;
            if (config.speed != null)
                hf.entity.boss.Bat.SPEED = config.speed;
            if (config.wait_time != null)
                hf.entity.boss.Bat.WAIT_TIME = hf.Data.SECOND * config.wait_time;
        });

        // TODO: only do the following if modifies_rotating_fireball.

        this.change_fireball_constants = Ref.auto(BossFireBall.init).before(function(hf: Hf, self: BossFireBall, game: GameMode): Void {
            if (config.rotating_fireball_speed != null)
                self.turnSpeed = config.rotating_fireball_speed;
            if (config.rotating_fireball_global_distance != null)
                self.maxDist = hf.Data.CASE_WIDTH * config.rotating_fireball_global_distance;
            (cast self).sensRotation = 1;
        });

        this.replace_bat_rotating_fireballs = Ref.auto(hf.entity.boss.Bat.bossAnger).replace(function(hf: Hf, self: hf.entity.boss.Bat): Void {
            /* Same as original except for removed "attachFireBall". */
            self.stopWait();
            self.halt();
            self.playAnim(hf.Data.ANIM_BAT_ANGER);
            self.game.fxMan.attachExplosion(self.x, self.y, 60);
            self.fl_anger = true;
            self.setNext(null, null, hf.Data.SECOND * config.delay_to_hit_bat, hf.Data.ACTION_MOVE);

            /* Custom */
            if (config.rotating_fireball_list != null) {
                for (fireball_data in config.rotating_fireball_list) {
                    self.attachFireBall(fireball_data[0], fireball_data[1]);
                }
            }
        });

        this.direction_aware_fireball_init = Ref.auto(BossFireBall.initBossShoot).replace(function(hf: Hf, self: BossFireBall, bat: hf.entity.boss.Bat, angle: Float): Void {
            self.bat = bat;
            self.ang = angle * Math.PI / 180;
            (cast self).sensRotation = (angle < 0) ? -1 : 1;
            self.center();
        });

        this.direction_aware_fireball_center = Ref.auto(BossFireBall.center).replace(function(hf: Hf, self: BossFireBall): Void {
            self.moveTo(self.bat.x + Math.cos(self.ang) * self.dist * (cast self).sensRotation,
            self.bat.y + Math.sin(self.ang) * self.dist);
        });

        // TODO: only do the following if launches_fireball.

        this.prepare_to_launch_fireballs = Ref.auto(hf.entity.boss.Bat.halt).before(function(hf: Hf, self: hf.entity.boss.Bat): Void {
            (cast self).timeBeforeNextFireball = config.delay_before_first_launched_fireball;
            (cast self).nbFireballToLaunch = config.nb_launched_fireballs;
            (cast self).fireballAngle = Std.random(360);
            (cast self).fireballSensRotation = 1 - 2 * Std.random(2);
        });

        this.launch_fireballs = Ref.auto(hf.entity.boss.Bat.update).before(function(hf: Hf, self: hf.entity.boss.Bat): Void {
            if (!self.fl_wait)
                return;

            if ((cast self).nbFireballToLaunch <= 0)
                return;

            if ((cast self).timeBeforeNextFireball <= 0) {
                var current_angle = (cast self).fireballAngle;
                for (i in 0...config.nb_launched_fireball_at_once) {
                    var fireball = hf.entity.shoot.FireBall.attach(self.game, self.x, self.y);
                    fireball.moveToAng(current_angle, config.launched_fireball_speed);
                    current_angle += config.launched_fireball_internal_angle;
                }
                (cast self).nbFireballToLaunch--;
                (cast self).fireballAngle += config.angle_between_launched_fireballs * (cast self).fireballSensRotation;
                (cast self).timeBeforeNextFireball = config.delay_between_launched_fireballs;
            }
            (cast self).timeBeforeNextFireball--;
        });

        this.bat_in_dim = Ref.auto(hf.mode.Adventure.startLevel).after(function(hf: Hf, self: hf.mode.Adventure): Void {
            if (self.world.currentId == hf.Data.BAT_LEVEL) {
                hf.entity.boss.Bat.attach(self);
                self.getOne(hf.Data.BOSS).register(hf.Data.BAD_CLEAR);
                self.fl_clear = false;
            }
        });

        this.bat_in_dim_bosslevel = Ref.auto(hf.mode.Adventure.isBossLevel).postfix(function(hf: Hf, self: hf.mode.Adventure, id: Int, oldret: Bool): Bool {
            return (id == hf.Data.BAT_LEVEL);
        });

        this.bat_in_dim_checkclear = Ref.auto(hf.mode.GameMode.checkLevelClear).postfix(function(hf: Hf, self: hf.mode.GameMode, oldret: Bool): Bool {
            if (self.world.currentId == hf.Data.BAT_LEVEL && !self.fl_clear) {
                return false;
            }
            return oldret;
        });
    }
}
