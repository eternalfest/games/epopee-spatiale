package items;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.IItem;
import vault.ISpec;
import custom_clips.CustomClips;

import items.specs.Letter;

@:build(patchman.Build.di())
class Items {
    @:diExport
    public var spec(default, null): ISpec;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new(clips: CustomClips,
                        patches: Array<IPatch>) {
        this.patches = FrozenArray.from(patches);

        this.spec = new Letter();

    }
}
