package items.specs;

import merlin.value.MerlinFloat;
import etwin.Obfu;
import merlin.Merlin;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import patchman.Ref;
import vault.ISpec;

@:build(patchman.Build.di())
class Letter implements ISpec {
    public var id(default, null): Int = 118;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {}

    public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item: hf.entity.Item): Void {
        Merlin.setLevelVar(specMan.game.world.current, Obfu.raw("NOTE"), new MerlinFloat(1));
    }

    public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void {
    }
}
