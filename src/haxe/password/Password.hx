package password;

import password.actions.PasswordTrigger;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import etwin.flash.Key;
import patchman.Ref;

@:build(patchman.Build.di())
class Password {

    @:diExport
    public var action(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var secondLastLetter: Int = 0;
    public var lastLetter: Int = 0;

    public function new() {
        this.action = new PasswordTrigger(this);

        var patches = [

        ];

        this.patches = FrozenArray.from(patches);
    }

    public function passwordTrigger(game: hf.mode.GameMode): Bool {
        for (i in 65...90) {
            if (Key.isDown(i) && game.keyLock != i) {
                game.keyLock = i;
                if (this.lastLetter != i) {
                    this.secondLastLetter = this.lastLetter;
                    this.lastLetter = i;
                }
            }
        }

        if (this.secondLastLetter == 65 && this.lastLetter == 78) {
            return true;
        }
        return false;
    }
}
