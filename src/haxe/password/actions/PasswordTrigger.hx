package password.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class PasswordTrigger implements IAction {
    public var name(default, null): String = Obfu.raw("password");
    public var isVerbose(default, null): Bool = false;

    private var mod: Password;

    public function new(mod: Password) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();

        return this.mod.passwordTrigger(game);
    }
}
