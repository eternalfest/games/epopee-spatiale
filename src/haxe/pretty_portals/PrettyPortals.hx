package pretty_portals;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import hf.Hf;
import hf.mode.GameMode.PortalMc;
import hf.mode.GameMode;
import merlin.IAction;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.Ref;
import pretty_portals.actions.OpenPortalColor;
import pretty_portals.actions.OpenPortalHsv;

@:build(patchman.Build.di())
class PrettyPortals {
  public static var PORTALS(default, null): WeakMap<PortalMc, Float> = new WeakMap();

  public static var PATCH(default, null) = Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
    for (p in self.portalMcList) {
      var hue: Null<Float> = PORTALS.get(p);
      if (hue != null) {

        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(hue, 1, 1).toFilter();
        var nextHue: Float = (hue + 1) % 360;

        p.mc.filters = [filter];
        PrettyPortals.PORTALS.set(p, nextHue);
      }
    }
  });

  @:diExport
  public var patch(default, null): IPatch;
  @:diExport
  public var openPortalColor(default, null): IAction;
  @:diExport
  public var openPortalHsv(default, null): IAction;

  public function new(): Void {
    this.patch = PATCH;
    this.openPortalColor = new OpenPortalColor();
    this.openPortalHsv = new OpenPortalHsv();
  }
}
