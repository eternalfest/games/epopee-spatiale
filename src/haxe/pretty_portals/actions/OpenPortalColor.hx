package pretty_portals.actions;

import hf.mode.GameMode.PortalMc;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;
import pretty_portals.PrettyPortals;

class OpenPortalColor implements IAction {
  public var name(default, null): String = Obfu.raw("openPortalColor");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var pid: Int = ctx.getInt(Obfu.raw("pid"));

    var game: GameMode = ctx.getGame();
    game.openPortal(x, y, pid);

    var p: PortalMc = game.portalMcList[pid];
    Assert.debug(p != null);

    PrettyPortals.PORTALS.set(p, 0);

    return false;
  }
}
