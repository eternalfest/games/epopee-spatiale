package pretty_portals.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import hf.mode.GameMode.PortalMc;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class OpenPortalHsv implements IAction {
  public var name(default, null): String = Obfu.raw("openPortalHsv");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var pid: Int = ctx.getInt(Obfu.raw("pid"));
    var h: Float = ctx.getFloat(Obfu.raw("h"));
    var s: Float = ctx.getFloat(Obfu.raw("s"));
    var v: Float = ctx.getFloat(Obfu.raw("v"));

    var game: GameMode = ctx.getGame();

    game.openPortal(x, y, pid);
    var p: PortalMc = game.portalMcList[pid];
    Assert.debug(p != null);

    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(h, s / 100, v / 100).toFilter();
    p.mc.filters = [filter];

    return false;
  }
}
