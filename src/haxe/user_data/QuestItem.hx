package user_data;
class QuestItem {
    public static var STATE_BASE: Int = 0;
    public static var STATE_NOT_STARTED: Int = 1;
    public static var STATE_STARTED: Int = 2;
    public static var STATE_OVER: Int = 3;

    public var id: Int;
    public var name: String;
    public var description: String;
    public var require: Map<Int, Int>;
    public var give: Array<RewardItem>;
    public var remove: Array<RewardItem>;
    public var state: Int;
    public function new() {
        require = new Map<Int, Int>();
        give = new Array<RewardItem>();
        remove = new Array<RewardItem>();
        state = STATE_BASE;
    }

    public function get_state(inventory: Inventory): Int {
        if(state == STATE_BASE) {
            var completed: Int = 0;
            var total: Int = 0;
            for(key in require.keys()) {
                total++;
                var n : Int = require[key];
                if(inventory.get(key) != null && inventory.get(key) >= n)
                    completed++;
            }
            if(completed == total)
                state = STATE_OVER;
            else if(completed > 0)
                state = STATE_STARTED;
            else
                state = STATE_NOT_STARTED;
        }
        return state;
    }
}
