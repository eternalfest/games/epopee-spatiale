package user_data;
import hf.FxManager;
import patchman.PatchList;
import patchman.Ref;
import hf.mode.GameMode;
import etwin.Obfu;
import patchman.HostModuleLoader;
import patchman.IPatch;

@:build(patchman.Build.di())
class UserData {

    private static var instance: UserData;
    public var quests: Array<QuestItem>;

    public var required_quests: Map<Int, Array<QuestItem>>;

    public var inventory:Inventory;
    @:diExport
    public var patch(default, null): IPatch;
    public function new(patches: Array<IPatch>, hml: HostModuleLoader, data : patchman.module.Data) {
        instance = this;
        quests = new Array<QuestItem>();
        required_quests = new Map<Int, Array<QuestItem>>();
        var loaded: Bool = false;
        var quests: Array<Dynamic> = data.get(Obfu.raw("Quests"));

        patches.push(Ref.auto(hf.mode.GameMode.initWorld).before(function(hf, self: GameMode) {
            if(!loaded) {
                inventory = new Inventory(get_inventory(hml), self);
                loaded = true;
                for(quest in quests) {
                    var item = new QuestItem();
                    item.id = Std.parseInt(Reflect.field(quest, Obfu.raw("id")));
                    item.name = Reflect.field(quest, Obfu.raw("name"));
                    item.description = Reflect.field(quest, Obfu.raw("description"));
                    var require: Dynamic = Reflect.field(quest, Obfu.raw("require"));
                    for(i in Reflect.fields(require)) {
                        var n : Int = Reflect.field(require, i);
                        var key: Int = Std.parseInt(i);
                        item.require[key] = n;
                        if(required_quests[key] == null)
                            required_quests[key] = new Array<QuestItem>();
                        required_quests[key].push(item);
                    }

                    var give: Dynamic = Reflect.field(quest, Obfu.raw("give"));
                    var remove: Dynamic = Reflect.field(quest, Obfu.raw("remove"));

                    for(i in 0...Reflect.fields(give).length) {
                        var structure : Dynamic = Reflect.field(give, cast i);
                        var reward = new RewardItem();
                        for(j in Reflect.fields(structure)) {
                            var val : String = Reflect.field(structure, j);
                            var key: String = j;
                            reward.reward[key] = val;
                        }
                        item.give.push(reward);
                    }

                    for(i in 0...Reflect.fields(remove).length) {
                        var structure : Dynamic = Reflect.field(remove, cast i);
                        var reward = new RewardItem();
                        for(j in Reflect.fields(structure)) {
                            var val : String = Reflect.field(structure, j);
                            var key: String = j;
                            reward.reward[key] = val;
                        }
                        item.remove.push(reward);
                    }
                    this.quests.push(item);
                }
            }
        }));

        this.patch = new PatchList(patches);
    }


    public static function get_inventory(hml: HostModuleLoader): Map<Int, Int> {
        var start: Dynamic = hml.require("run-start");
        var data: Null<Dynamic> = Reflect.callMethod(start, Reflect.field(start, Obfu.raw("get")), []);
        if (data == null) {
            return null;
        }
        var items: Dynamic = Reflect.field(data, Obfu.raw("items"));
        var map: Map<Int, Int> = new Map<Int, Int>();
        for(j in Reflect.fields(items)) {
            var val : Int = Std.parseInt(Reflect.field(items, j));
            var key: Int = Std.parseInt(j);
            map[key] = val;
        }
        return map;
    }

    public static function get(): UserData {
        return instance;
    }
}
